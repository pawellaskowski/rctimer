package com.pjl.rctimer.fragment;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pjl.rctimer.R;
import com.pjl.rctimer.view.SolveOptionsView;
import com.pjl.rctimer.utilities.SolvesDatabaseHelper;
import com.pjl.rctimer.model.TimerState;
import com.pjl.rctimer.utilities.Util;
import com.pjl.rctimer.model.PuzzleType;
import com.pjl.rctimer.model.Solve;
import com.pjl.rctimer.activity.MainActivity;
import com.pjl.rctimer.activity.SettingsActivity;

import net.gnehzr.tnoodle.scrambles.Puzzle;

import java.util.Date;

import static com.pjl.rctimer.model.TimerState.*;



public class TimerFragment extends Fragment {

    String scramble;
    boolean isScrambleReady;
    Puzzle puzzle;

    RelativeLayout fragmentLayout;
    TextView timerView;
    ImageView optionsExpander;
    LinearLayout solveOptionsLayout;
    TextView scrambleView;
    ImageView reScrambleView;
    SolveOptionsView solveOptionsView;
    ActionBar toolbar;
    ProgressBar progressBar;
    BottomNavigationView bottomNavigationView;

    Solve currentSolve;
    PuzzleType puzzleType;

    TimerState mState = IDLE;

    long mStartTime;
    long mInterval = 0;
    long mHoldDuration = 500;
    boolean mInspectionEnabled;
    boolean mHoldEnabled;
    int mInspectionDuration = 15;
    long mInspectionStartTime;
    long timeElapsed;
    int penalty = Solve.PENALTY_NONE;
    Handler handler;
    GenerateScrambleTask generateScrambleTask;


    private SolvesDatabaseHelper dbHelper;

    public TimerFragment() {
        // Required empty public constructor
    }

    public static TimerFragment newInstance() {
        return new TimerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = SolvesDatabaseHelper.getInstance(getActivity());
        handler = new Handler();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (generateScrambleTask != null) {
            generateScrambleTask.cancel(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String puzzleTypeArg = getArguments().getString(MainActivity.KEY_PUZZLE_TYPE);
        View rootView = inflater.inflate(R.layout.fragment_timer, container, false);
        fragmentLayout = (RelativeLayout) rootView.findViewById(R.id.timer_fragment_layout);
        timerView = (TextView) rootView.findViewById(R.id.timer);
        optionsExpander = (ImageView) rootView.findViewById(R.id.expand_options_arrow);
        solveOptionsLayout = (LinearLayout) rootView.findViewById(R.id.expand_options);
        reScrambleView = (ImageView) rootView.findViewById(R.id.re_scramble);
        reScrambleView.setVisibility(View.GONE);
        scrambleView = (TextView) rootView.findViewById(R.id.scramble);
        toolbar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        bottomNavigationView = (BottomNavigationView) getActivity().findViewById(R.id.bottom_navigation);
        fragmentLayout.setOnTouchListener(new MyTouchListener());
        puzzleType = PuzzleType.valueOf(puzzleTypeArg);
        puzzle = puzzleType.getPuzzle();
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        generateScramble();

        solveOptionsView = (SolveOptionsView) rootView.findViewById(R.id.solve_options);
        solveOptionsLayout.setVisibility(View.GONE);

        timerView.setText(R.string.zero_time);

        solveOptionsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (solveOptionsView.getVisibility() == View.GONE) {
                    showSolveOptions();
                } else {
                    hideSolveOptions();
                }
            }
        });

        reScrambleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateScramble();
            }
        });

        return rootView;
    }

    public void generateScramble() {
        generateScrambleTask = new GenerateScrambleTask();
        generateScrambleTask.execute();
    }

    private class GenerateScrambleTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isScrambleReady = false;
            reScrambleView.setVisibility(View.GONE);
            scrambleView.setText(R.string.generating_scramble);
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(Void... voids) {
            return puzzle.generateScramble();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            scramble = s;
            scrambleView.setText(scramble);
            reScrambleView.setVisibility(View.VISIBLE);
            isScrambleReady = true;
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mInspectionEnabled = sharedPref.getBoolean(SettingsActivity.KEY_PREF_INSPECTION, false);
        mHoldEnabled = sharedPref.getBoolean(SettingsActivity.KEY_PREF_HOLD, true);
        mInspectionDuration = Integer.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_INSPECTION_TIME, "15"));
        if (mHoldEnabled) {
            mHoldDuration = Integer.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_HOLD_TIME, "500"));
        } else {
            mHoldDuration = 1;
        }
    }

    private void showSolveOptions() {
        solveOptionsView.setVisibility(View.VISIBLE);
        optionsExpander.setImageResource(R.drawable.ic_expand_less);
    }

    private void hideSolveOptions() {
        solveOptionsView.setVisibility(View.GONE);
        optionsExpander.setImageResource(R.drawable.ic_expand_more);
    }

    private class MyTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            TimerState state = getState();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    switch(state) {
                        case IDLE:
                            if (isScrambleReady) {
                                if (isInspectionEnabled()) {
                                    startInspection();
                                    hideUI();
                                } else {
                                    startHold();
                                }
                            }
                            hideSolveOptions();
                            return true;

                        case INSPECTION:
                            startHold();
                            return true;

                        case RUNNING:
                            stop();
                            showUI();

                            generateScramble();
                            return true;
                    }

                case MotionEvent.ACTION_UP:
                    switch (state) {
                        case IDLE:
                            stopHold();
                            return true;

                        case INSPECTION:
                            stopHold();
                            return true;

                        case READY:
                            start();
                            hideUI();
                            return true;
                    }
                default:
                    return false;
            }

        }

        private final Runnable mInspectionRunnable = new Runnable() {
            @Override
            public void run() {
                long inspectionTimeElapsed = SystemClock.elapsedRealtime() - mInspectionStartTime;
                long seconds = inspectionTimeElapsed / 1000;
                long remaining = mInspectionDuration - seconds;

                if (remaining == -2) {
                    timerView.setText(R.string.dnf);
                    penalty = Solve.PENALTY_DNF;
                    setState(IDLE);
                    stopInspection();
                    saveSolve();
                    showUI();
                } else {
                    if (remaining == 0 || remaining == -1) {
                        timerView.setText(R.string.plustwo);
                        penalty = Solve.PENALTY_PLUSTWO;
                    } else {
                        timerView.setText(String.valueOf(remaining));
                    }
                    handler.postDelayed(mInspectionRunnable, 1000);
                }
            }
        };

        private final Runnable mTimerRunnable = new Runnable() {

            @Override
            public void run() {
                timeElapsed = SystemClock.elapsedRealtime() - mStartTime;
                if (getState() == RUNNING) {
                    timerView.setText(Util.getFormattedStringFromTime(timeElapsed));
                    handler.postDelayed(mTimerRunnable, mInterval);
                } else {
                    if (timeElapsed < mHoldDuration) {
                        handler.postDelayed(mTimerRunnable, mInterval);
                    } else {
                        setState(READY);
                        timerView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGreen));
                        if (! mInspectionEnabled) {
                            timerView.setText(R.string.zero_time);
                        }
                        stopHold();
                    }
                }
            }
        };

        private void start() {
            if (mInspectionEnabled) {
                stopInspection();
            }
            setState(RUNNING);
            mStartTime = SystemClock.elapsedRealtime();
            timerView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorBlack));
            handler.post(mTimerRunnable);
        }

        private void stop() {
            setState(IDLE);
            handler.removeCallbacks(mTimerRunnable);
            saveSolve();
        }

        private void saveSolve() {
            if (penalty == Solve.PENALTY_PLUSTWO) {
                timeElapsed += 2000;
                timerView.setText(Util.getFormattedStringFromTime(timeElapsed));
                timerView.append("+");
            } else if (penalty == Solve.PENALTY_DNF) {
                timeElapsed = 0;
            }
            currentSolve = saveCurrentSolveToDB();
            solveOptionsView.setSolve(currentSolve);
        }

        private void startHold() {
            penalty = Solve.PENALTY_NONE;
            mStartTime = SystemClock.elapsedRealtime();
            handler.post(mTimerRunnable);
        }

        private void stopHold() {
            handler.removeCallbacks(mTimerRunnable);
        }

        private void startInspection() {
            setState(INSPECTION);
            mInspectionStartTime = SystemClock.elapsedRealtime();
            handler.post(mInspectionRunnable);
        }

        private void stopInspection() {
            handler.removeCallbacks(mInspectionRunnable);
        }

        private boolean isInspectionEnabled() {
            return mInspectionEnabled;
        }

        private TimerState getState() {
            return mState;
        }

        private void setState(TimerState state) {
            mState = state;
        }

        private void hideUI() {
            toolbar.hide();
            scrambleView.setVisibility(View.GONE);
            bottomNavigationView.setVisibility(View.GONE);
            solveOptionsLayout.setVisibility(View.GONE);
            reScrambleView.setVisibility(View.GONE);
        }

        private void showUI() {
            toolbar.show();
            scrambleView.setVisibility(View.VISIBLE);
            bottomNavigationView.setVisibility(View.VISIBLE);
            solveOptionsLayout.setVisibility(View.VISIBLE);
        }

        private Solve saveCurrentSolveToDB() {
            Solve solve = new Solve(scramble, timeElapsed, penalty, puzzleType, new Date());
            long solveId = dbHelper.addSolveAndGetId(solve);
            solve.setId(solveId);
            return solve;
        }

    }

}
