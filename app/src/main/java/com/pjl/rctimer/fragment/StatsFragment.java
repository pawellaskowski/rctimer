package com.pjl.rctimer.fragment;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pjl.rctimer.R;
import com.pjl.rctimer.utilities.SolvesDatabaseHelper;
import com.pjl.rctimer.utilities.Util;
import com.pjl.rctimer.model.PuzzleType;
import com.pjl.rctimer.model.Solve;
import com.pjl.rctimer.activity.MainActivity;

public class StatsFragment extends Fragment {
    public static final long RESULT_NOT_AVAILABLE = -1;
    public static final long RESULT_DNF = 0;

    PuzzleType puzzleType;
    SolvesDatabaseHelper dbHelper;
    Cursor mCursor;
    View rootView;


    public StatsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static StatsFragment newInstance() {
        StatsFragment fragment = new StatsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = SolvesDatabaseHelper.getInstance(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stats, container, false);
        String puzzleTypeArg = getArguments().getString(MainActivity.KEY_PUZZLE_TYPE);
        puzzleType = PuzzleType.valueOf(puzzleTypeArg);
        mCursor = dbHelper.getSolves(puzzleType);
        setStats();
        return rootView;
    }

    private void setStats() {
        TextView currentAo5 = (TextView) rootView.findViewById(R.id.current_ao5);
        TextView bestAo5 = (TextView) rootView.findViewById(R.id.best_ao5);
        TextView currentAo12 = (TextView) rootView.findViewById(R.id.current_ao12);
        TextView bestAo12 = (TextView) rootView.findViewById(R.id.best_ao12);
        TextView currentAo100 = (TextView) rootView.findViewById(R.id.current_ao100);
        TextView bestAo100 = (TextView) rootView.findViewById(R.id.best_ao100);
        TextView currentAo1000 = (TextView) rootView.findViewById(R.id.current_ao1000);
        TextView bestAo1000 = (TextView) rootView.findViewById(R.id.best_ao1000);
        TextView numberOfSolves = (TextView) rootView.findViewById(R.id.number_of_solves);
        TextView bestSolve = (TextView) rootView.findViewById(R.id.best_solve);


        currentAo5.setText(Util.getFormattedStringFromAverage(getActivity(), calculateAverageOfLastN((5))));
        bestAo5.setText(Util.getFormattedStringFromAverage(getActivity(), calculateBestAverageOfN(5)));
        currentAo12.setText(Util.getFormattedStringFromAverage(getActivity(), calculateAverageOfLastN(12)));
        bestAo12.setText(Util.getFormattedStringFromAverage(getActivity(), calculateBestAverageOfN(12)));
        currentAo100.setText(Util.getFormattedStringFromAverage(getActivity(), calculateAverageOfLastN(100)));
        bestAo100.setText(Util.getFormattedStringFromAverage(getActivity(), calculateBestAverageOfN(100)));
        currentAo1000.setText(Util.getFormattedStringFromAverage(getActivity(), calculateAverageOfLastN(1000)));
        bestAo1000.setText(Util.getFormattedStringFromAverage(getActivity(), calculateBestAverageOfN(1000)));
        numberOfSolves.setText(String.valueOf(mCursor.getCount()));
        bestSolve.setText(Util.getFormattedStringFromAverage(getActivity(), calculateBestTime()));

    }

    private long calculateBestAverageOfN(int n) {
        if (mCursor.getCount() < n) {
            return RESULT_NOT_AVAILABLE;
        }
        long best = Long.MAX_VALUE;
        for (int i = 0; i <= mCursor.getCount() - n; i++) {
            long average = calculateAverageOfN(n, i);
            if (average != RESULT_DNF) {
                if (average < best) {
                    best = average;
                }
            }
        }
        if (best == Long.MAX_VALUE) {
            return RESULT_DNF;
        }

        return best;
    }

    private long calculateAverageOfLastN(int n) {
        return calculateAverageOfN(n, 0);
    }

    private long calculateAverageOfN(int n, int startIndex) {
        long result = 0;

        int solveCount = mCursor.getCount();
        if (solveCount < n) {
            return RESULT_NOT_AVAILABLE;
        }
        if (startIndex <= solveCount - n) {
            mCursor.moveToPosition(startIndex);
            int dnfCount = 0;
            long bestTime = Long.MAX_VALUE;
            long worstTime = 0;
            for (int i = 0; i < n; i++) {
                long solveTime = mCursor.getLong(mCursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_TIME));
                int penalty = mCursor.getInt(mCursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_PENALTY));

                if (penalty == Solve.PENALTY_DNF || solveTime == 0) {
                    dnfCount++;
                    if (dnfCount > 1) {
                        return RESULT_DNF;
                    }
                } else {
                    result += solveTime;
                    if (solveTime < bestTime) {
                        bestTime = solveTime;
                    }
                    if (solveTime > worstTime) {
                        worstTime = solveTime;
                    }
                }
                mCursor.moveToNext();
            }
            if (dnfCount == 1) {
                result = (result - bestTime)/(n - 2);
            } else {
                result = (result - bestTime - worstTime) / (n - 2);
            }
        }
        return result;
    }

    private long calculateBestTime() {
        if (mCursor.getCount() == 0) {
            return RESULT_NOT_AVAILABLE;
        }
        long bestTime = Long.MAX_VALUE;
        mCursor.moveToFirst();
        for (int i = 0; i < mCursor.getCount(); i++) {
            long solveTime = mCursor.getLong(mCursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_TIME));
            int penalty = mCursor.getInt(mCursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_PENALTY));
            if (solveTime != 0 && penalty != Solve.PENALTY_DNF) {
                if (solveTime < bestTime) {
                    bestTime = solveTime;
                }
            }
            mCursor.moveToNext();
        }
        if (bestTime == Long.MAX_VALUE) {
            return RESULT_NOT_AVAILABLE;
        }
        return bestTime;
    }

}
