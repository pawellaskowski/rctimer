package com.pjl.rctimer.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pjl.rctimer.R;
import com.pjl.rctimer.activity.MainActivity;
import com.pjl.rctimer.databinding.HistoryListItemBinding;
import com.pjl.rctimer.model.PuzzleType;
import com.pjl.rctimer.model.Solve;
import com.pjl.rctimer.utilities.SolvesDatabaseHelper;

import java.util.Date;

public class HistoryFragment extends Fragment {

    PuzzleType puzzleType;

    SolvesDatabaseHelper dbHelper;
    RecyclerView recyclerView;
    HistoryRecyclerViewCursorAdapter adapter;

    public HistoryFragment() {
        // Required empty public constructor
    }

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = SolvesDatabaseHelper.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);

        String puzzleTypeArg = getArguments().getString(MainActivity.KEY_PUZZLE_TYPE);
        puzzleType = PuzzleType.valueOf(puzzleTypeArg);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);
        adapter = new HistoryRecyclerViewCursorAdapter(getActivity(), dbHelper.getSolves(puzzleType));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        checkForEmptyList(rootView);

        return rootView;
    }

    private void checkForEmptyList(View view) {
        if (adapter.getItemCount() == 0) {
            TextView emptyListTextView = (TextView) view.findViewById(R.id.empty_list);
            emptyListTextView.setText(getResources().getString(R.string.empty_list_text));
            emptyListTextView.setVisibility(View.VISIBLE);
        }
    }


    public class HistoryRecyclerViewCursorAdapter extends RecyclerView.Adapter<HistoryRecyclerViewCursorAdapter.ViewHolder> {

        Context mContext;
        Cursor mCursor;

        public HistoryRecyclerViewCursorAdapter(Context context, Cursor cursor) {
            mContext = context;
            mCursor = cursor;
        }

        @Override
        public HistoryRecyclerViewCursorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.history_list_item, parent, false);
            HistoryRecyclerViewCursorAdapter.ViewHolder holder = new HistoryRecyclerViewCursorAdapter.ViewHolder(view);
            holder.itemView.setTag(holder);
            return holder;
        }


        @Override
        public void onBindViewHolder(final HistoryRecyclerViewCursorAdapter.ViewHolder holder, int position) {
            mCursor.moveToPosition(position);
            holder.bindCursor(mCursor);
            holder.expandedView.setVisibility(View.GONE);
            holder.expandIcon.setImageResource(R.drawable.ic_expand_more);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean shouldExpand = holder.expandedView.getVisibility() == View.GONE;
                    ChangeBounds transition = new ChangeBounds();
                    transition.setDuration(50);
                    if (shouldExpand) {
                        holder.expandedView.setVisibility(View.VISIBLE);
                        holder.expandIcon.setImageResource(R.drawable.ic_expand_less);

                    } else {
                        holder.expandedView.setVisibility(View.GONE);
                        holder.expandIcon.setImageResource(R.drawable.ic_expand_more);

                    }
                    TransitionManager.beginDelayedTransition(recyclerView, transition);
                    holder.itemView.setActivated(shouldExpand);
                }
            });



            holder.noteTextView.setText(holder.solve.getNote());

            if (holder.solve.getNote() != null && ! holder.solve.getNote().equals("")) {
                holder.noteImageView.setVisibility(View.VISIBLE);
                holder.noteTextView.setVisibility(View.VISIBLE);
            } else {
                holder.noteImageView.setVisibility(View.INVISIBLE);
                holder.noteTextView.setVisibility(View.GONE);
            }

            if (holder.solve.getPenalty() == Solve.PENALTY_PLUSTWO ) {
                holder.penaltyLabel.setVisibility(View.VISIBLE);
                holder.penaltyLabel.setText(R.string.plustwo);
            } else if (holder.solve.getPenalty() == Solve.PENALTY_DNF) {
                holder.penaltyLabel.setVisibility(View.VISIBLE);
                holder.penaltyLabel.setText(R.string.dnf);
            } else {
                holder.penaltyLabel.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return mCursor.getCount();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            HistoryListItemBinding binding;
            TextView scrambleTextView;
            TextView noteTextView;
            LinearLayout expandedView;
            TextView deleteButton;
            TextView noteButton;
            TextView penaltyButton;
            Solve solve;
            int selectedPenalty;
            ImageView noteImageView;
            TextView penaltyLabel;
            ImageView expandIcon;

            public ViewHolder(View itemView) {
                super(itemView);
                binding = DataBindingUtil.bind(itemView);
                scrambleTextView = (TextView) itemView.findViewById(R.id.solve_scramble);
                noteTextView = (TextView) itemView.findViewById(R.id.note);
                expandedView = (LinearLayout) itemView.findViewById(R.id.expanded_item);
                deleteButton = (TextView) itemView.findViewById(R.id.solve_delete);
                noteButton = (TextView) itemView.findViewById(R.id.solve_note);
                penaltyButton = (TextView) itemView.findViewById(R.id.solve_penalty);
                noteImageView = (ImageView) itemView.findViewById(R.id.note_image);
                penaltyLabel = (TextView) itemView.findViewById(R.id.penalty_label);
                expandIcon = (ImageView) itemView.findViewById(R.id.expand_more);

                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogTheme)
                                .setTitle(R.string.delete_lc)
                                .setMessage(R.string.solve_delete_confirmation)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dbHelper.deleteSolve(solve.getId());
                                        mCursor = dbHelper.getSolves(puzzleType);
                                        adapter.notifyItemRemoved(getAdapterPosition());
                                        Toast.makeText(getActivity(), R.string.solve_deleted, Toast.LENGTH_SHORT).show();
                                    }})
                                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .show();
                    }
                });

                noteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogTheme_Text);
                        builder.setTitle(R.string.note);

                        final EditText input = new EditText(getActivity());
                        if (solve.getNote() != null) {
                            input.setText(solve.getNote());
                            input.post(new Runnable() {
                                @Override
                                public void run() {
                                    input.setSelection(input.getText().length());
                                }
                            });
                        }

                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        builder.setView(input);
                        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                solve.setNote(input.getText().toString());
                                dbHelper.updateSolve(solve);
                                mCursor = dbHelper.getSolves(puzzleType);
                                adapter.notifyItemChanged(getAdapterPosition());
                                Toast.makeText(getActivity(), R.string.note_saved, Toast.LENGTH_SHORT).show();
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        final AlertDialog dialog = builder.create();
                        input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                                }
                            }
                        });
                        dialog.show();
                    }
                });

                penaltyButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int penalty = solve.getPenalty();

                        CharSequence[] penalties = {
                                getResources().getString(R.string.none),
                                getResources().getString(R.string.plustwo),
                                getResources().getString(R.string.dnf)
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogTheme);
                        builder.setTitle(R.string.penalty);

                        builder.setSingleChoiceItems(penalties, penalty, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedPenalty = which;
                            }
                        });

                        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (selectedPenalty != solve.getPenalty()) {
                                    if (selectedPenalty == Solve.PENALTY_PLUSTWO) {
                                        long newTime = solve.getTime() + 2000;
                                        solve.setTime(newTime);
                                    } else {
                                        if (solve.getPenalty() == Solve.PENALTY_PLUSTWO) {
                                            long newTime = solve.getTime() - 2000;
                                            solve.setTime(newTime);
                                        }
                                    }
                                    solve.setPenalty(selectedPenalty);
                                }
                                dbHelper.updateSolve(solve);
                                mCursor = dbHelper.getSolves(puzzleType);
                                adapter.notifyItemChanged(getAdapterPosition());
                                Toast.makeText(getActivity(), R.string.penalty_set, Toast.LENGTH_SHORT).show();
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();

                    }
                });
            }

            public void bindCursor(Cursor cursor) {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_ID));
                long time = cursor.getLong(cursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_TIME));
                String scramble = cursor.getString(cursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_SCRAMBLE));
                Date date = new Date(cursor.getLong(cursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_DATE)));
                PuzzleType puzzleType = PuzzleType.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_PUZZLE_TYPE)));
                int penalty = cursor.getInt(cursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_PENALTY));
                String note = cursor.getString(cursor.getColumnIndexOrThrow(SolvesDatabaseHelper.SOLVE_NOTE));
                Solve solve = new Solve(scramble,time,penalty, puzzleType, date);
                solve.setNote(note);
                solve.setId(id);
                binding.setSolve(solve);
                this.solve = solve;
            }
        }
    }

}
