package com.pjl.rctimer.utilities;

import android.content.Context;

import com.pjl.rctimer.R;
import com.pjl.rctimer.fragment.StatsFragment;
import com.pjl.rctimer.model.Solve;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    static DateFormat df = new SimpleDateFormat("dd/MM HH:mm");

    public static String getFormattedStringFromTime(long time) {
        long millis = (time % 1000);
        long second = (time / 1000) % 60;
        long minute = (time / (1000 * 60)) % 60;

        if (minute == 0 && second < 10) {
            return String.format("%01d.%03d", second, millis);
        } else if (minute == 0 && second >= 10) {
            return String.format("%02d.%03d", second, millis);
        } else if (minute <10) {
            return String.format("%01d:%02d.%03d", minute, second, millis);
        } else {
            return String.format("%02d:%02d.%03d", minute, second, millis);
        }

        //return String.format("%02d:%02d.%03d", minute, second, millis);
    }

    public static String displayFullTimeOrDnf(Context context, Solve solve) {
        if (solve.getPenalty() == Solve.PENALTY_DNF) {
            return context.getString(R.string.dnf);
        } else {
            return getFullFormattedStringFromTime(solve.getTime());
        }
    }

    public static String displayTimeOrDnf(Context context, Solve solve) {
        if (solve.getPenalty() == Solve.PENALTY_DNF) {
            return context.getString(R.string.dnf);
        } else {
            return getFormattedStringFromTime(solve.getTime());
        }
    }

    public static String getFullFormattedStringFromTime(long time) {
        long millis = (time % 1000);
        long second = (time / 1000) % 60;
        long minute = (time / (1000 * 60)) % 60;
        return String.format("%02d:%02d.%03d", minute, second, millis);
    }

    public static String removePlusAtTheEndOfString(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length()-1)=='+') {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    public static String getFormattedStringFromAverage(Context context, long time) {
        String result;
        if (time == StatsFragment.RESULT_NOT_AVAILABLE) {
            result =  context.getString(R.string.not_available);
        } else if (time == StatsFragment.RESULT_DNF) {
            result = context.getString(R.string.dnf);
        } else {
            result = getFullFormattedStringFromTime(time);
        }
        return result;
    }

    public static String getDate(Date date) {
        return df.format(date);
    }
}
