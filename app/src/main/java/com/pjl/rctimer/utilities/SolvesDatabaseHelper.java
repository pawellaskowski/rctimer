package com.pjl.rctimer.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pjl.rctimer.model.PuzzleType;
import com.pjl.rctimer.model.Solve;

public class SolvesDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DBlog";

    // Database Info
    public static final String DATABASE_NAME = "solvesDatabase";
    public static final int DATABASE_VERSION = 1;

    // Table Names
    public static final String TABLE_SOLVES = "solves";

    // Solves Table Columns
    public static final String SOLVE_ID = "_id";
    public static final String SOLVE_TIME = "solveTime";
    public static final String SOLVE_SCRAMBLE = "scramble";
    public static final String SOLVE_PUZZLE_TYPE = "puzzleType";
    public static final String SOLVE_PENALTY = "penalty";
    public static final String SOLVE_NOTE = "note";
    public static final String SOLVE_DATE = "date";

    // Singleton
    private static SolvesDatabaseHelper sInstance;

    public static synchronized SolvesDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SolvesDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public SolvesDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_SOLVES_TABLE = "CREATE TABLE " + TABLE_SOLVES +
                "(" +
                SOLVE_ID + " INTEGER PRIMARY KEY," +
                SOLVE_TIME + " INTEGER," +
                SOLVE_PENALTY + " INTEGER," +
                SOLVE_SCRAMBLE + " TEXT," +
                SOLVE_PUZZLE_TYPE + " TEXT," +
                SOLVE_NOTE + " TEXT," +
                SOLVE_DATE + " INTEGER" +
                ")";

        sqLiteDatabase.execSQL(CREATE_SOLVES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    public long addSolveAndGetId(Solve solve) {
        SQLiteDatabase db = getWritableDatabase();
        long id = -1;
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(SOLVE_SCRAMBLE, solve.getScramble());
            values.put(SOLVE_TIME, solve.getTime());
            values.put(SOLVE_PUZZLE_TYPE, solve.getPuzzleType().name());
            values.put(SOLVE_DATE, solve.getDate().getTime());
            values.put(SOLVE_PENALTY, solve.getPenalty());

            id = db.insertOrThrow(TABLE_SOLVES, null, values);

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add solve to database");
        } finally {
            db.endTransaction();
        }
        return id;
    }

    public void deleteSolve(long id) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_SOLVES, SOLVE_ID + " = ?", new String[] {String.valueOf(id)});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while deleting solve from database");
        } finally {
            db.endTransaction();
        }
    }

    public void updateSolve(Solve solve) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(SOLVE_TIME, solve.getTime());
            values.put(SOLVE_NOTE, solve.getNote());
            values.put(SOLVE_PENALTY, solve.getPenalty());

            db.update(TABLE_SOLVES, values, SOLVE_ID + " = ?", new String[] {String.valueOf(solve.getId())});

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while updating solve");
        } finally {
            db.endTransaction();
        }

    }

    public Cursor getSolves(PuzzleType puzzleType) {

        String SOLVES_SELECT_QUERY =
                String.format("SELECT * FROM %s WHERE %s = %s ORDER BY %s DESC",
                        TABLE_SOLVES,
                        SOLVE_PUZZLE_TYPE,
                        "'" + puzzleType.name() + "'",
                        SOLVE_DATE);

        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery(SOLVES_SELECT_QUERY, null);
    }

}
