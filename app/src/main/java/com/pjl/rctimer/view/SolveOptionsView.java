package com.pjl.rctimer.view;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pjl.rctimer.R;
import com.pjl.rctimer.model.Solve;
import com.pjl.rctimer.utilities.SolvesDatabaseHelper;
import com.pjl.rctimer.utilities.Util;


public class SolveOptionsView extends GridLayout {
    Button dnfButton;
    Button plusTwoButton;
    Button deleteButton;
    Button noteButton;
    TextView timerView;
    View rootView;
    private Solve solve;
    long solveTime;
    SolvesDatabaseHelper dbHelper;

    public SolveOptionsView(Context context) {
        this(context, null );
    }

    public SolveOptionsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SolveOptionsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(final Context context) {
        dbHelper = SolvesDatabaseHelper.getInstance(context);
        rootView = inflate(context, R.layout.solve_options, this);
        dnfButton = (Button) rootView.findViewById(R.id.dnf_button);
        plusTwoButton = (Button) rootView.findViewById(R.id.plustwo_button);
        deleteButton = (Button) rootView.findViewById(R.id.delete_button);
        noteButton = (Button) rootView.findViewById(R.id.note_button);


        dnfButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                timerView = (TextView) v.getRootView().findViewById(R.id.timer);
                if (solve.getPenalty() == Solve.PENALTY_NONE || solve.getPenalty() == Solve.PENALTY_PLUSTWO) {
                    timerView.setText(R.string.dnf);
                    solve.setPenalty(Solve.PENALTY_DNF);
                } else {
                    timerView.setText(Util.getFormattedStringFromTime(solveTime));
                    solve.setPenalty(Solve.PENALTY_NONE);
                }
                solve.setTime(solveTime);
                dbHelper.updateSolve(solve);
            }
        });

        plusTwoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                timerView = (TextView) v.getRootView().findViewById(R.id.timer);
                if (solve.getPenalty() == Solve.PENALTY_NONE || solve.getPenalty() == Solve.PENALTY_DNF) {
                    solve.setPenalty(Solve.PENALTY_PLUSTWO);
                    solve.setTime(solveTime + 2000);
                    timerView.setText(Util.getFormattedStringFromTime(solve.getTime()));
                    timerView.append("+");

                } else {
                    solve.setTime(solveTime);
                    timerView.setText(Util.removePlusAtTheEndOfString(Util.getFormattedStringFromTime(solveTime)));
                    solve.setPenalty(Solve.PENALTY_NONE);
                }
                dbHelper.updateSolve(solve);

            }
        });

        deleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                timerView = (TextView) v.getRootView().findViewById(R.id.timer);
                final LinearLayout solveOptions = (LinearLayout) v.getRootView().findViewById(R.id.expand_options);

                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyAlertDialogTheme);
                builder.setTitle(R.string.delete_lc);
                builder.setMessage(R.string.solve_delete_confirmation);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dbHelper.deleteSolve(solve.getId());
                        Toast.makeText(context, R.string.solve_deleted, Toast.LENGTH_SHORT).show();
                        timerView.setText(R.string.zero_time);
                        SolveOptionsView.this.setVisibility(GONE);
                        solveOptions.setVisibility(GONE);

                }});
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        noteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyAlertDialogTheme);
                builder.setTitle(R.string.note);

                final EditText input = new EditText(context);
                if (solve.getNote() != null) {
                    input.setText(solve.getNote());
                    input.post(new Runnable() {
                        @Override
                        public void run() {
                            input.setSelection(input.getText().length());
                        }
                    });
                }

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        solve.setNote(input.getText().toString());
                        dbHelper.updateSolve(solve);
                        Toast.makeText(context, R.string.note_saved, Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog dialog = builder.create();

                input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        }
                    }
                });
                dialog.show();

            }
        });

    }

    public void setSolve(Solve solve) {
        this.solve = solve;
        solveTime = solve.getTime();
        if (solve.getPenalty() == Solve.PENALTY_PLUSTWO) {
            solveTime -= 2000;
        }
    }

}
