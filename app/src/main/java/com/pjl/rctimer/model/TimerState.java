package com.pjl.rctimer.model;

public enum TimerState {
    IDLE, INSPECTION, READY, RUNNING
}
