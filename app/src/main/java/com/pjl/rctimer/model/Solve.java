package com.pjl.rctimer.model;

import java.util.Date;

public class Solve {

    // Penalties
    public static final int PENALTY_NONE = 0;
    public static final int PENALTY_PLUSTWO = 1;
    public static final int PENALTY_DNF = 2;


    private long id;
    private String scramble;
    private long time;
    private int penalty;
    private PuzzleType puzzleType;
    private String note;
    private Date date;

    public Solve() {}

    public Solve(String scramble, long time, int penalty, PuzzleType puzzleType, Date date) {
        this.scramble = scramble;
        this.time = time;
        this.penalty = penalty;
        this.puzzleType = puzzleType;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public String getScramble() {
        return scramble;
    }

    public PuzzleType getPuzzleType() {
        return puzzleType;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
