package com.pjl.rctimer.model;

import net.gnehzr.tnoodle.scrambles.Puzzle;

import puzzle.ClockPuzzle;
import puzzle.CubePuzzle;
import puzzle.MegaminxPuzzle;
import puzzle.PyraminxPuzzle;
import puzzle.SkewbPuzzle;
import puzzle.SquareOneUnfilteredPuzzle;

public enum PuzzleType {

    CUBE3("3x3x3", new CubePuzzle(3)),
    CUBE2("2x2x2", new CubePuzzle(2)),
    CUBE4("4x4x4", new CubePuzzle(4)),
    CUBE5("5x5x5", new CubePuzzle(5)),
    CUBE6("6x6x6", new CubePuzzle(6)),
    CUBE7("7x7x7", new CubePuzzle(7)),
    CLOCK("Clock", new ClockPuzzle()),
    MEGAMINX("Megaminx", new MegaminxPuzzle()),
    PYRAMINX("Pyraminx", new PyraminxPuzzle()),
    SKEWB("Skewb", new SkewbPuzzle()),
    SQUARE1("Square-1", new SquareOneUnfilteredPuzzle()),
    CUBE3OH("One Hand 3x3x3", new CubePuzzle(3)),
    CUBE3BF("Blindfold 3x3x3", new CubePuzzle(3)),
    CUBE4BF("Blindfold 4x4x4", new CubePuzzle(4)),
    CUBE5BF("Blindfold 5x5x5", new CubePuzzle(5));

    private String displayedName;
    private Puzzle puzzle;

    PuzzleType(String name, Puzzle puzzle) {
        this.displayedName = name;
        this.puzzle = puzzle;
    }

    public String getDisplayedName() {
        return displayedName;
    }

    public Puzzle getPuzzle() {
        return puzzle;
    }

}
