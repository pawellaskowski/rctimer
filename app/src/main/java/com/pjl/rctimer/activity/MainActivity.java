package com.pjl.rctimer.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.pjl.rctimer.R;
import com.pjl.rctimer.fragment.HistoryFragment;
import com.pjl.rctimer.fragment.StatsFragment;
import com.pjl.rctimer.fragment.TimerFragment;
import com.pjl.rctimer.model.PuzzleType;


public class MainActivity extends AppCompatActivity {

    Spinner mSpinner;
    PuzzleType selectedPuzzle = PuzzleType.CUBE3;
    Fragment current;

    public static final String KEY_PUZZLE_TYPE = "PUZZLE_TYPE";
    final String PREF_PUZZLE = "PREF_PUZZLE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        setUpBottomNavigation();
        setUpToolbar();

        current = TimerFragment.newInstance();
        provideSelectedPuzzleToFragment();
        replaceFragmentWithCurrent();
    }

    private void setUpToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        PuzzleAdapter adapter = new PuzzleAdapter(this, R.layout.puzzletype_spinner_item, PuzzleType.values());
        adapter.setDropDownViewResource(R.layout.puzzletype_spinner_item);
        mSpinner = (Spinner) findViewById(R.id.puzzletype_spinner);
        mSpinner.setAdapter(adapter);
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        int defaultSpinnerChoice = sharedPref.getInt(PREF_PUZZLE, 0);
        mSpinner.setSelection(defaultSpinnerChoice);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPuzzle = (PuzzleType) parent.getItemAtPosition(position);
                if (current instanceof TimerFragment) {
                    current = TimerFragment.newInstance();
                } else if (current instanceof HistoryFragment) {
                    current = HistoryFragment.newInstance();
                } else {
                    current = StatsFragment.newInstance();
                }
                provideSelectedPuzzleToFragment();
                replaceFragmentWithCurrent();
                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(PREF_PUZZLE, position);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpBottomNavigation () {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.action_timer:
                        current = TimerFragment.newInstance();
                        break;
                    case R.id.action_history:
                        current = HistoryFragment.newInstance();
                        break;
                    case R.id.action_stats:
                        current = StatsFragment.newInstance();
                        break;
                }
                provideSelectedPuzzleToFragment();
                replaceFragmentWithCurrent();
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void provideSelectedPuzzleToFragment() {
        Bundle args = new Bundle();
        args.putString(KEY_PUZZLE_TYPE, selectedPuzzle.toString());
        current.setArguments(args);
    }

    private void replaceFragmentWithCurrent() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, current);
        transaction.commit();
    }

    private class PuzzleAdapter extends ArrayAdapter<PuzzleType> {

        PuzzleType[] puzzleTypes;
        LayoutInflater inflater;

        private PuzzleAdapter(Context context, int resource, PuzzleType[] puzzleTypes) {
            super(context, resource, puzzleTypes);
            this.puzzleTypes = puzzleTypes;
            inflater = getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.puzzletype_spinner_item, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.puzzletype_name);
            textView.setText(puzzleTypes[position].getDisplayedName());
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            convertView = inflater.inflate(R.layout.puzzletype_spinner_item, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.puzzletype_name);
            textView.setText(puzzleTypes[position].getDisplayedName());
            return convertView;
        }
    }


}
